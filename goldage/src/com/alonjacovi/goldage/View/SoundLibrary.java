package com.alonjacovi.goldage.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public enum SoundLibrary implements Library
{
	PLACEHOLDER("world/placeholder.wav");
	
	//** no dispose method - SFX are used everywhere in the entire game
	//to add specific SFX that are only used in certain positions, create another enum
	
	private String path;
	private Sound sound;
	
	private SoundLibrary(String path)
	{
		this.path = path;
		
		sound = Gdx.audio.newSound(Gdx.files.internal(path));
	}
	
	@Override
	public String getPath()
	{
		return path;
	}
	
	public void play() {
		sound.play();
	}

}
