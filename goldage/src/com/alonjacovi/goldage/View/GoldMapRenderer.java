package com.alonjacovi.goldage.View;

import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;

public class GoldMapRenderer implements Renderer
{
	private GoldMap world;
	
	//music
	private Library currentMusic;
	private Music music;
	
	//rendering tools
	OrthographicCamera cam;
	TextureAtlas atlas;
	
	public GoldMapRenderer(GoldMap world) {
		this.world = world;
		
		//initial music playback
		currentMusic = world.getCurrentMusic();
		music = Gdx.audio.newMusic(Gdx.files.internal(currentMusic.getPath()));
		music.setLooping(true);
		//music.play();
		
		//Camera set to (0,0) being bottom left
		float h = Gdx.graphics.getHeight();
		float w = Gdx.graphics.getWidth();
		cam = new OrthographicCamera(w, h);
		cam.position.set(w/2, h/2, 0);
		//following 2 lines are for zoom 2x, remove to revert
		cam.viewportHeight = h/4;
		cam.viewportWidth = w/4;
		
		cam.update();
		
		atlas = new TextureAtlas(Gdx.files.internal(world.getTextures().getPath()));
	}
	
	@Override
	public void playMusic() 
	{
		//TODO fade in/out for music
		Library song;
		if (currentMusic != (song = world.getCurrentMusic()) )
		{
			currentMusic = song;
			music.dispose();
			music = Gdx.audio.newMusic(Gdx.files.internal(currentMusic.getPath()));
			music.setLooping(true);
			//music.play();
		}
	}
	
	SpriteBatch batch = new SpriteBatch();
	OrthogonalTiledMapRenderer renderer;
	
	@Override
	public void render()
	{
		playMusic();
		
		//clearing the screen
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//TODO divide into layers
		renderer = new OrthogonalTiledMapRenderer(world.getMap());
		cam.update();
		renderer.setView(cam);
		renderer.render();
		
		batch.setProjectionMatrix(cam.combined);
		
		float playerPosX = (world.getPlayer().getPosition().x*world.getTileSize());
		float playerPosY = (world.getPlayer().getPosition().y*world.getTileSize());
		
		cam.position.set(new Vector2(playerPosX, playerPosY), 0);
		//cam.position.lerp(new Vector3(playerPosX, playerPosY, 0), 0.3f);
		cam.update();
		
		batch.begin();

		for (Entity entity : world.getEntities())
		{
			entity.getAnimator().render(atlas, world.getTileSize(), batch, entity);
		}
		
		world.getPlayer().getAnimator()
			.render(atlas, world.getTileSize(), batch, world.getPlayer());
		
		batch.end();
	}
	
	@Override
	public void dispose()
	{
		atlas.dispose();
		music.dispose();
		renderer.dispose();
	}
	
	
}
