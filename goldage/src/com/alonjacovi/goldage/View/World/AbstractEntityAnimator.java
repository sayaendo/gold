package com.alonjacovi.goldage.View.World;

import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class AbstractEntityAnimator implements EntityAnimator
{
	private Animation moveLeft, moveRight, moveUp, moveDown;
	private Animation idleLeft, idleRight, idleUp, idleDown;
	
	private boolean isCached = false;
	private Animation lastMovement;
	
	private final float frameBuffer = 0.17f;
	

	@Override
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch, Entity entity) {
		
		batch.draw(getTexture(atlas, entity), entity.getPosition().x * tileSize,
														entity.getPosition().y * tileSize);
	}

	@Override
	public TextureRegion getTexture(TextureAtlas atlas, Entity entity) {
		
		if (!isCached)
		{
			moveLeft = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"walkingLeft"));
			moveRight = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"walkingRight"));
			moveUp = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"walkingUp"));
			moveDown = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"walkingDown"));
			idleLeft = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"idleLeft"));
			idleRight = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"idleRight"));
			idleUp = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"idleUp"));
			idleDown = new Animation(frameBuffer, atlas.findRegions(entity.getCode()+"idleDown"));
			isCached = true;
		}
		
		if (entity.isMovingLeft())
		{
			lastMovement = moveLeft;
			return moveLeft.getKeyFrame(entity.getFsm(0).getTimer(), true);
		}
		if (entity.isMovingRight())
		{
			lastMovement = moveRight;
			return moveRight.getKeyFrame(entity.getFsm(0).getTimer(), true);
		}
		if (entity.isMovingUp())
		{
			lastMovement = moveUp;
			return moveUp.getKeyFrame(entity.getFsm(0).getTimer(), true);
		}
		if (entity.isMovingDown())
		{
			lastMovement = moveDown;
			return moveDown.getKeyFrame(entity.getFsm(0).getTimer(), true);
		}
		
		//TODO add movement history to the FSMs (as a caching vector that is checked here with isMoving___)
		if (lastMovement == moveLeft)
			return idleLeft.getKeyFrame(entity.getFsm(0).getTimer(), true);
		if (lastMovement == moveRight)
			return idleRight.getKeyFrame(entity.getFsm(0).getTimer(), true);
		if (lastMovement == moveDown)
			return idleDown.getKeyFrame(entity.getFsm(0).getTimer(), true);
		if (lastMovement == moveUp)
			return idleUp.getKeyFrame(entity.getFsm(0).getTimer(), true);
		
		return idleDown.getKeyFrame(entity.getFsm(0).getTimer(), true);
		}

}
