package com.alonjacovi.goldage.View.World;

import com.alonjacovi.goldage.Controller.GoldInputProcessor;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.View.Renderer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class WorldScreen implements Screen
{
	GoldMap world;
	Renderer renderer;
	
	public WorldScreen(GoldMap world) {
		this.world = world;
		this.renderer = world.getRenderer();
		
		Gdx.input.setInputProcessor(new GoldInputProcessor(world));
	}

	@Override
	public void render(float delta)
	{
		world.update(delta);
		renderer.render();
	}

	@Override
	public void resize(int width, int height)
	{
		//not supporting android
	}

	@Override
	public void show()
	{
		//not supporting android
	}

	@Override
	public void hide()
	{
		//not supporting android
	}

	@Override
	public void pause()
	{
		//not supporting android
	}

	@Override
	public void resume()
	{
		//not supporting android
	}

	@Override
	public void dispose()
	{
		world.dispose();
		renderer.dispose();
	}

}
