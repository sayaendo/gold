package com.alonjacovi.goldage.View;

public interface Renderer
{
	//constructor takes world, creates sprites array, tile renderer, plays music
	
	//checks if the current playing music should change, changes if so
	public void playMusic();
	
	//render method for the main loop
	//renders tiles, playMusic(), all entities of the world, and player
	//renders tile layers by hardcoded order of their property "type"
	public void render();
	
	//disposes all assets, remember to dispose even those that only have variables inside the render() method!
	public void dispose();

}
