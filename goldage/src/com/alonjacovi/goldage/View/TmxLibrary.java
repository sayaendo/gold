package com.alonjacovi.goldage.View;

public enum TmxLibrary implements Library
{
	PLACEHOLDER("world/placeholder.tmx"),
	LEVEL_PLACEHOLDER("world/placeholderLevel.tmx");
	
	private String path;
	
	private TmxLibrary(String path) {
		this.path = path;
	}

	@Override
	public String getPath() {
		return path;
	}

}
