package com.alonjacovi.goldage.View.Level;

import com.alonjacovi.goldage.Controller.GoldInputProcessor;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.View.Renderer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class LevelScreen implements Screen
{
	
	GoldMap level;
	Renderer renderer;
	
	public LevelScreen(GoldMap level) {
		this.level = level;
		this.renderer = level.getRenderer();
		
		Gdx.input.setInputProcessor(new GoldInputProcessor(level));
	}

	@Override
	public void render(float delta)
	{
		level.update(delta);
		renderer.render();
	}

	@Override
	public void resize(int width, int height)
	{
		//not supporting android
	}

	@Override
	public void show()
	{
		//not supporting android
	}

	@Override
	public void hide()
	{
		//not supporting android
	}

	@Override
	public void pause()
	{
		//not supporting android
	}

	@Override
	public void resume()
	{
		//not supporting android
	}

	@Override
	public void dispose()
	{
		level.dispose();
		renderer.dispose();
	}

	
}