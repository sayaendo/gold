package com.alonjacovi.goldage.View.Level;

import com.alonjacovi.goldage.Controller.Level.PlayerFsm.PlayerMovementState;
import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class PlayerAnimator implements EntityAnimator
{
	//TODO dispose of this through the renderer and dispose() in tne EntityAnimator
	
	//render decides on an offset based on player state
	//windup and crouch attack have offsets to add or remove from x based on direction facing (4 possible values besides 0)
	//jumping up = crouch, jumping down = walking 2
	
	//change walking to 4 frames including the idle one
	
	private Animation idleLeft, idleRight, swingLeft, swingRight, 
			walkingRight, walkingLeft, attackRight, attackLeft,
			crouchAttackRight, crouchAttackLeft, crouchingRight, crouchingLeft;
	
	private boolean isCached = false;
	private final float frameBuffer = 0.17f/2;

	@Override
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch,
			Entity entity) {
		
		batch.draw(getTexture(atlas, entity),  (entity.getPosition().x*tileSize), (entity.getPosition().y*tileSize));

	}

	@Override
	public TextureRegion getTexture(TextureAtlas atlas, Entity entity) 
	{	
		int code = entity.getCode();
		
		TextureRegion tempRegion = atlas.findRegion(code+"idleLeft");
		TextureRegion tempRegion2 = atlas.findRegion(code+"idleRight");
		
		Array<TextureRegion> tempArray = new Array<TextureRegion>();
		tempArray.add(tempRegion);
		Array<TextureRegion> tempArray2 = new Array<TextureRegion>();
		tempArray2.add(tempRegion2);
		
		if (!isCached)
		{
			idleLeft = new Animation(frameBuffer, atlas.findRegions(code+"idleLeft"));
			idleRight = new Animation(frameBuffer, atlas.findRegions(code+"idleRight"));
			swingLeft = new Animation(frameBuffer, atlas.findRegions(code+"swingLeft"));
			swingRight = new Animation(frameBuffer, atlas.findRegions(code+"swingRight"));
			attackLeft = new Animation(frameBuffer, atlas.findRegions(code+"attackLeft"));
			attackRight = new Animation(frameBuffer, atlas.findRegions(code+"attackRight"));
			
			tempArray.addAll(atlas.findRegions(code+"walkingLeft"));
			walkingLeft = new Animation(frameBuffer, tempArray);
			
			tempArray2.addAll(atlas.findRegions(code+"walkingRight"));
			walkingRight = new Animation(frameBuffer, tempArray2);
			
			crouchingLeft = new Animation(frameBuffer, atlas.findRegions(code+"crouchingLeft"));
			crouchingRight = new Animation(frameBuffer, atlas.findRegions(code+"crouchingRight"));
			crouchAttackLeft = new Animation(frameBuffer, atlas.findRegions(code+"crouchAttackLeft"));
			crouchAttackRight = new Animation(frameBuffer, atlas.findRegions(code+"crouchAttackRight"));
			isCached = true;
		}
		
		float timer = entity.getFsm(0).getTimer();
		
		switch ((PlayerMovementState) entity.getFsm(0).getState())
		{
		case IDLE: 
			
			if (entity.getDirection().x >= 0)
				return idleRight.getKeyFrame(timer, true);
			else return idleLeft.getKeyFrame(timer, true);

		case WALKING_RIGHT:
		case WALKING_LEFT:
			
			if (entity.getDirection().x >= 0)
				return walkingRight.getKeyFrame(timer, true);
			else return walkingLeft.getKeyFrame(timer, true);
			
		case JUMPING:
			
			if (entity.getDirection().x >= 0)
			{
				if (entity.getDirection().y < 0)
					return atlas.findRegion(code+"walkingRight", 2);
				else return atlas.findRegion(code+"crouchingRight");
			} else {
				if (entity.getDirection().y < 0)
					return atlas.findRegion(code+"walkingLeft", 2);
				else return atlas.findRegion(code+"crouchingLeft");
			}
			
		case LANDING:
			
			if (entity.getDirection().x >= 0)
				return crouchingRight.getKeyFrame(timer, true);
			else return crouchingLeft.getKeyFrame(timer, true);
			
		case CROUCHING:
			
			if (entity.getDirection().x >= 0)
				return crouchingRight.getKeyFrame(timer, true);
			else return crouchingLeft.getKeyFrame(timer, true);
			
		case AIR_SUSPEND:
			
			return atlas.findRegion(code+"walkingRight", 2);
			
		case ATTACKING:
			break;
		case CROUCH_ATTACKING:
			break;
		case WINDUP:
			break;
		case RECOIL:
			break;
		}
		
		throw new IllegalStateException("unknown player state while rendering");
	}
	
	


}
