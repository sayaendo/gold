package com.alonjacovi.goldage.View.Level;

import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GruntAnimator implements EntityAnimator
{

	@Override
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch,
			Entity entity) {
		
		batch.draw(getTexture(atlas, entity), entity.getPosition().x*tileSize, entity.getPosition().y*tileSize);

	}

	@Override
	public TextureRegion getTexture(TextureAtlas atlas, Entity entity) {
		return atlas.findRegion(entity.getCode()+"idleDown");
	}
}
