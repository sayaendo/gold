package com.alonjacovi.goldage.View;

public enum TexturesLibrary implements Library
{
	PLACEHOLDER("world/placeholderEntities.pack"),
	LEVEL_PLACEHOLDER("world/sidescroller.pack");
	
	private String path;
	
	private TexturesLibrary(String path) {
		this.path = path;
	}

	@Override
	public String getPath() {
		return path;
	}

}
