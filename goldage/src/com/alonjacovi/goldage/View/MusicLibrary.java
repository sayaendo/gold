package com.alonjacovi.goldage.View;

public enum MusicLibrary implements Library
{
	PLACEHOLDER("world/placeholder.ogg"),
	PLACEHOLDER2("world/placeholder2.mp3");
	
	private String path;
	
	private MusicLibrary(String path) {
		this.path = path;
	}
	
	@Override
	public String getPath() {
		return path;
	}
	
	public static MusicLibrary getMusic(String name) {
		for (MusicLibrary song : MusicLibrary.values())
		{
			if (song.toString().equals(name))
				return song;
		}
		throw new IllegalArgumentException("invalid song name "+name);
	}

}
