package com.alonjacovi.goldage.View;

import com.alonjacovi.goldage.Model.Entity;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public interface EntityAnimator
{
	
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch, Entity entity);
	public TextureRegion getTexture(TextureAtlas atlas, Entity entity);
	
}
