package com.alonjacovi.goldage.Model;

import com.alonjacovi.goldage.View.Library;
import com.alonjacovi.goldage.View.Renderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;

public interface GoldMap
{
	//constructor uses getTMX to load its tileset
	//constructor also takes in player's initial position on load
	
	//takes the music layer from the tileset, iterates over the library and compares the object's property to toString
	public Library getCurrentMusic();
	public boolean isMusicChanged();
	
	//returns implementation's renderer
	public Renderer getRenderer();
	
	//input methods
	public void handleKeyPressed(int keycode);
	public void handleKeyReleased(int keycode);
	
	public TiledMap getMap();
	//tile size as multiplier to deal with objects placement and rendering calculations
	public int getTileSize();
	public Library getTextures();
	
	public Array<Entity> getEntities();
	public Entity getPlayer();
	
	//removes all enemies that are dead, then calls update on all of them except those that are DORMANT
	public void update(float delta);
	
	//calls the specific world's state machine's update()
	//called from inside the update() method
//	public void updateState();
	
	//dispose and reload the tile set
	//when switching away from the World while persisting it, and returning to the persisting World, you must dispose and reload assets manually
	public void dispose();
	public void reload();
	
}
