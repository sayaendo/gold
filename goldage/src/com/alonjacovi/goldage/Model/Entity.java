package com.alonjacovi.goldage.Model;

import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public interface Entity
{
	//mutable with public control: the respective state machines should have full control over their entities
	//vectors on grid with (0,0) bottom left
	public Vector2 getPosition();
	public Vector2 getVelocity();
	public Vector2 getAcceleration();
	
	public void setPosition(Vector2 pos);
	public void setVelocity(Vector2 vel);
	public void setAcceleration(Vector2 acc);
	
	public Vector2 getDirection();
	
	//returns the rectangle the size of a single tile. for entities in other sizes, override this method
	public Rectangle getBounds();
	
	//registered final int code to match the image sprite's name
	public int getCode();
	
	//state machine interfacing:
	
	//gets called by world every frame
	//calls the entity's own state machine's update()
	public void update(float delta, GoldMap map);
	//calls the entity's own state machine's handleInput
	public void handleKeyPressed(int keycode, GoldMap map);
	public void handleKeyReleased(int keycode, GoldMap map);
	
	//updates its own position and velocity (and clamps velocity here)
	public void updatePosition(float delta); //TODO remove this, FSM should update position because of different clamps
	public void resetMovement();
	
	//returns the position after the update
	public Vector2 tryUpdate();
	
	//state machine inputting for the implementation
	public void setFsms();
	
	public EntityFsm getFsm(int index);
	
	public boolean isMovingLeft();
	public boolean isMovingRight();
	public boolean isMovingUp();
	public boolean isMovingDown();
	
	public boolean isCollidingLeft();
	public boolean isCollidingRight();
	public boolean isCollidingUp();
	public boolean isCollidingDown();
	
	public EntityAnimator getAnimator();
	
}
