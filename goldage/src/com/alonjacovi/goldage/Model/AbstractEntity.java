package com.alonjacovi.goldage.Model;

import com.alonjacovi.goldage.Collision;
import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class AbstractEntity implements Entity
{
	private final int code;
	
	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleration;
	
	private Rectangle bounds;
	
	protected Array<EntityFsm> fsms;
	
	private EntityAnimator animator;
	
	public AbstractEntity(int code, Vector2 pos, Vector2 vel, Vector2 acc, Rectangle bounds, Array<EntityFsm> fsms, EntityAnimator animator) {
		this.code = code;
		
		this.position = pos;
		this.velocity = vel;
		this.acceleration = acc;
		
		this.bounds = bounds;
		
		this.fsms = fsms;
		
		this.animator = animator;
	}
	
	@Override
	public int getCode() {
		return code;
	}

	@Override
	public void setFsms() {
		// TODO remove this
	}

	@Override
	public EntityAnimator getAnimator() {
		return animator;
	}

	@Override
	public Vector2 getPosition() {
		return position;
	}
	
	@Override
	public Vector2 getVelocity() {
		return velocity;
	}
	
	@Override
	public Vector2 getAcceleration() {
		return acceleration;
	}
	
	@Override
	public void setPosition(Vector2 pos) {
		this.position = pos;
	}
	
	@Override
	public void setVelocity(Vector2 vel) {
		this.velocity = vel;
	}
	
	@Override
	public void setAcceleration(Vector2 acc) {
		this.acceleration = acc;
	}
	
	protected float maxSpeed = 1/10f;
	
	//TODO remove this and update manually in World FSM
	@Override
	public void updatePosition(float delta) {
		this.position.add(velocity);
		//this.position.lerp(position.cpy().add(velocity), delta);
		this.velocity.add(acceleration);
		
		if (velocity.x > maxSpeed) velocity.x = maxSpeed;
		//if (velocity.y > 1/7.5f) velocity.y = 1/7.5f; 
		if (velocity.x < -maxSpeed) velocity.x = -maxSpeed;
		//if (velocity.y < -1/7.5f) velocity.y = -1/7.5f; 
	}
	
	private Vector2 direction = new Vector2();
	
	@Override
	public Vector2 getDirection() {
		return direction;
	}
	
	@Override
	public void resetMovement() {
		this.velocity.x = 0;
		this.velocity.y = 0;
		this.acceleration.x = 0;
		this.acceleration.y = 0;
	}
	
	@Override
	public Vector2 tryUpdate()
	{
		Vector2 newPosition = this.position.cpy();
		return newPosition.add(velocity);
	}
	
	@Override
	public boolean isMovingLeft() {
		if (getVelocity().x < 0)
			return true;
		return false;
	}

	@Override
	public boolean isMovingRight() {
		if (getVelocity().x > 0)
			return true;
		return false;
	}

	@Override
	public boolean isMovingUp() {
		if (getVelocity().y > 0)
			return true;
		return false;
	}

	@Override
	public boolean isMovingDown() {
		if (getVelocity().y < 0)
			return true;
		return false;
	}
	
	@Override
	public Rectangle getBounds()
	{
		return bounds;
	}
	
	//state machine interfacing
	
	@Override
	public void update(float delta, GoldMap map)
	{
		collidingLeft = false;
		collidingRight = false;
		collidingUp = false;
		collidingDown = false;
		if (isMovingLeft())
			collidingLeft = Collision.isCollidingLeft(map, this);
		if (isMovingRight())
			collidingRight = Collision.isCollidingRight(map, this);
		if (isMovingUp())
			collidingUp = Collision.isCollidingUp(map, this);
		if (isMovingDown())
			collidingDown = Collision.isCollidingDown(map, this);
		
		for (EntityFsm fsm : fsms)
			fsm.update(delta, map, this);
		
		if (velocity.x != 0)
			direction.x = Math.signum(velocity.x);
		if (velocity.y != 0)
			direction.y = Math.signum(velocity.y);
	}
	
	@Override
	public void handleKeyPressed(int keycode, GoldMap map) {
		for (EntityFsm fsm : fsms)
			fsm.handleKeyPressed(keycode, map, this);
	}


	@Override
	public void handleKeyReleased(int keycode, GoldMap map) {
		for (EntityFsm fsm : fsms)
			fsm.handleKeyReleased(keycode, map, this);
	}
	
	//FSM at index 0 is always the movement FSM with a timer for movement animation
	@Override
	public EntityFsm getFsm(int index) {
		return fsms.get(index);
	}
	
	private boolean collidingLeft;
	private boolean collidingRight;
	private boolean collidingUp;
	private boolean collidingDown;

	@Override
	public boolean isCollidingLeft() {
		return collidingLeft;
	}

	@Override
	public boolean isCollidingRight() {
		return collidingRight;
	}

	@Override
	public boolean isCollidingUp() {
		return collidingUp;
	}

	@Override
	public boolean isCollidingDown() {
		return collidingDown;
	}



}
