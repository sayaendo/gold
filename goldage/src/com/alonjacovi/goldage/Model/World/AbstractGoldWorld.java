package com.alonjacovi.goldage.Model.World;

import java.util.Iterator;

import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.View.Library;
import com.alonjacovi.goldage.View.MusicLibrary;
import com.alonjacovi.goldage.View.Renderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class AbstractGoldWorld implements GoldMap
{
	private TiledMap worldMap;
	private Player player;
	private Library currentMusic;
	private final int tileSize;
	
	private Library tmx;
	private Library textures;
	protected Renderer renderer;
	
	Array<Entity> entities = new Array<Entity>(20);
	
	public AbstractGoldWorld(Library tmx, Library textures, Vector2 playerPosition) 
	{
		this.tmx = tmx;
		this.textures = textures;
		player = Player.PlayerFactory(playerPosition);
		
		reload();
				
		TiledMapTileLayer layer = (TiledMapTileLayer) worldMap.getLayers().get(0);
		tileSize = (int) layer.getTileHeight();
		
		//taking the objects from the entities layer, and getting the entities from the translator
		//TODO only add entities on the screen
		MapLayer entitiesLayer = worldMap.getLayers().get("Entities");
		MapObjects objects = entitiesLayer.getObjects();
		Iterator<MapObject> iter = objects.iterator();
		while (iter.hasNext())
		{
			MapObject object = iter.next();
			if (object instanceof RectangleMapObject)
			{
				entities.add(WorldEntityTranslator.getFromRectangleObject((RectangleMapObject) object, getTileSize()));
			}
		}
	}
	
	@Override
	public Array<Entity> getEntities() {
		return entities;
	}
	
	@Override
	public void handleKeyPressed(int keycode) 
	{
		player.handleKeyPressed(keycode, this);
		
		//iterate all entities and call their keyPressed methods
	}
	
	@Override
	public void handleKeyReleased(int keycode)
	{
		player.handleKeyReleased(keycode, this);
		
		//same as above
	}
	
	@Override
	public Player getPlayer() {
		return player;
	}
	
	@Override
	public TiledMap getMap() {
		return worldMap;
	}
	
	@Override
	public int getTileSize() {
		return tileSize;
	}
	
	@Override
	public Library getCurrentMusic() {		
		//multiplying player position by tile size to compare with object placement
		int mul = getTileSize();
		Rectangle playerPosition = new Rectangle(player.getPosition().x*mul + mul/2, player.getPosition().y*mul + mul/2, 1, 1);
		
		MapLayer layer = worldMap.getLayers().get("Music");
		MapObjects objects = layer.getObjects();
		Iterator<MapObject> iter = objects.iterator();
		
		while (iter.hasNext())
		{
			MapObject object = iter.next();
			if (object instanceof RectangleMapObject)
			{
				Rectangle songArea = ((RectangleMapObject) object).getRectangle();
				if (playerPosition.overlaps(songArea))
				{
					return MusicLibrary.getMusic(object.getProperties().get("music", String.class));
				}
			} else if (object instanceof PolygonMapObject)
			{
				Polygon songArea = ((PolygonMapObject) object).getPolygon();
				if ((songArea.contains(playerPosition.x, playerPosition.y)) ||
					(songArea.contains(playerPosition.x+playerPosition.width, playerPosition.y)) ||
					(songArea.contains(playerPosition.x, playerPosition.y+playerPosition.height)) ||
					(songArea.contains(playerPosition.x+playerPosition.width, playerPosition.y+playerPosition.height)))
					
					return MusicLibrary.getMusic(object.getProperties().get("music", String.class));
			}

		}	
		throw new IllegalArgumentException("music layer in the TMX file is not fully covered");
	}
	
	@Override
	public boolean isMusicChanged() {
		Library music = getCurrentMusic();
		if (music == currentMusic)
			return false;
		else return true;
	}
	
	@Override
	public void update(float delta) 
	{
		//updates the specific world implementation's state machine
		//updateState();
		
		player.update(delta, this);
		
		//TODO don't call update on enemies that are DORMANT (were created but not on screen yet)
		
		for (Entity entity : entities)
		{
			entity.update(delta, this);
			
			if (entity.getFsm(0).getState().toString().equals("DEAD"))
				entities.removeValue(entity, true); //TODO change to for with int iterator and remove by index
		}
	}
	
	@Override
	public void dispose() {
		worldMap.dispose();
	}
	
	@Override
	public void reload() {
		worldMap = new TmxMapLoader().load(tmx.getPath()); 
	}
	
	@Override
	public Renderer getRenderer() {
		if (renderer == null)
			throw new NullPointerException(this+" is a GoldMap instance without a renderer");
		
		return renderer;
	}
	
	@Override
	public Library getTextures() {
		return textures;
	}
	

}
