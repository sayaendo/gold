package com.alonjacovi.goldage.Model.World;

import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.World.PlayerFsm;
import com.alonjacovi.goldage.Model.AbstractEntity;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.alonjacovi.goldage.View.World.PlayerAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Player extends AbstractEntity
{

	private Player(int code, Vector2 pos, Vector2 vel, Vector2 acc,
			Rectangle bounds, Array<EntityFsm> fsms, EntityAnimator animator)
	{
		super(code, pos, vel, acc, bounds, fsms, animator);
	}
	
	public static Player PlayerFactory(Vector2 pos) {
		
		int code = 1;
		Rectangle bounds = new Rectangle(pos.x, pos.y, 1, 1);
		Array<EntityFsm> fsms = new Array<EntityFsm>(1);
		fsms.add(new PlayerFsm());
		EntityAnimator animator = new PlayerAnimator();
		
		Player player = new Player(code, pos, new Vector2(), new Vector2(), bounds, fsms, animator);
		
		System.out.print(player);
		
		return player;
		
	}
	
}
