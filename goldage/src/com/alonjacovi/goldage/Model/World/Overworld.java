package com.alonjacovi.goldage.Model.World;

import com.alonjacovi.goldage.View.GoldMapRenderer;
import com.alonjacovi.goldage.View.Library;
import com.alonjacovi.goldage.View.TexturesLibrary;
import com.alonjacovi.goldage.View.TmxLibrary;
import com.badlogic.gdx.math.Vector2;

public class Overworld extends AbstractGoldWorld
{
	
	//TODO redo with state machine

	private Overworld(Library tmx, Library textures,
			Vector2 playerPosition)
	{
		super(tmx, textures, playerPosition);
		
		renderer = new GoldMapRenderer(this);
	}
	
	public static Overworld OverworldFactory(Vector2 playerPosition) {
		
		Library tmx = TmxLibrary.PLACEHOLDER;
		Library textures = TexturesLibrary.PLACEHOLDER;
		
		return new Overworld(tmx, textures, playerPosition);
	}

}
