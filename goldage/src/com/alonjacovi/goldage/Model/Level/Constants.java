package com.alonjacovi.goldage.Model.Level;

public enum Constants
{
	GRAVITY(1/60f),
	FRICTION(0.25f/45);
	
	private float value;
	
	private Constants(float value) {
		this.value = value;
	}
	
	public float get() {
		return value;
	}

}
