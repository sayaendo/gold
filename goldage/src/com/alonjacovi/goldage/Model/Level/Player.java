package com.alonjacovi.goldage.Model.Level;

import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.Level.PlayerFsm;
import com.alonjacovi.goldage.Controller.Level.PlayerWeaponFsm;
import com.alonjacovi.goldage.Model.AbstractEntity;
import com.alonjacovi.goldage.View.EntityAnimator;
import com.alonjacovi.goldage.View.Level.PlayerAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Player extends AbstractEntity
{

	private Player(int code, Vector2 pos, Vector2 vel, Vector2 acc,
			Rectangle bounds, Array<EntityFsm> fsms, EntityAnimator animator)
	{
		super(code, pos, vel, acc, bounds, fsms, animator);
		
		maxSpeed = 0.25f/2;
	}
	
	public static Player PlayerFactory(Vector2 pos) 
	{
		int code = 1;
		
		EntityAnimator animator = new PlayerAnimator();
		
		Array<EntityFsm> fsms = new Array<EntityFsm>(2);
		
		fsms.add(new PlayerFsm());
		fsms.add(new PlayerWeaponFsm());
		
		Rectangle bounds = new Rectangle(pos.x, pos.y, 1, 2);
		
		return new Player(code, pos, new Vector2(), new Vector2(), bounds, fsms, animator);
	}
	
	@Override
	public Rectangle getBounds() {
		return new Rectangle(getPosition().x, getPosition().y, 1, 2);
	}

}
