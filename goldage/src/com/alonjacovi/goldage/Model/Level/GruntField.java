package com.alonjacovi.goldage.Model.Level;

import com.alonjacovi.goldage.View.GoldMapRenderer;
import com.alonjacovi.goldage.View.Library;
import com.alonjacovi.goldage.View.TexturesLibrary;
import com.alonjacovi.goldage.View.TmxLibrary;
import com.badlogic.gdx.math.Vector2;


public class GruntField extends AbstractGoldLevel
{

	private GruntField(Library tmx, Library textures, Vector2 playerPosition)
	{
		super(tmx, textures, playerPosition);
		
		renderer = new GoldMapRenderer(this);
	}
	
	public static GruntField GruntFieldFactory(Vector2 playerPosition) {
		Library tmx = TmxLibrary.LEVEL_PLACEHOLDER;
		Library textures = TexturesLibrary.LEVEL_PLACEHOLDER;
		
		return new GruntField(tmx, textures, playerPosition);
	}

}
