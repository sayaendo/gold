package com.alonjacovi.goldage.Model.Level;

import com.alonjacovi.goldage.Model.Entity;
import com.badlogic.gdx.maps.objects.RectangleMapObject;

public enum LevelEntityTranslator
{
	INSTANCE;
	
	public static Entity getFromRectangleObject(RectangleMapObject object, int tileSize)
	{
		int code = Integer.parseInt(object.getProperties().get("code", String.class));
		
		switch (code)
		{
		case 1:
		case 2: 
			//return new GruntFsm(object, tileSize).getControlled();
		}
		
		throw new IllegalArgumentException("entity code is not registered");
	}

}
