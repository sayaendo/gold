package com.alonjacovi.goldage;

import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;

public enum Collision
{
	INSTANCE;
	
	//TODO remove the small bounce when colliding by looking at sonic and dermafan collision
	
	public static boolean isCollidingLeft(GoldMap map, Entity entity)
	{
		//GENERAL OUTLINE
		//1. get where the entity will be in the next frame
		//2. get the tile at that position (by rounding down the position using (int) cast from float)
		//3. if a tile exists at that location, there's collision
		
		//collision box is 1 pixel smaller than the real sprite
		float approx = 1f / (float) map.getTileSize();

		//1 and 2
		Rectangle bounds = entity.getBounds();
		Rectangle collisionBounds = new Rectangle(bounds.x + entity.getVelocity().x + approx, bounds.y + approx,
													bounds.width - approx*2, bounds.height - approx*2);

		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) map.getMap().getLayers().get("Collision");
		
		//iterates for as many times as there are tiles in the original bounds
		for (float i = collisionBounds.y; i <= (collisionBounds.y + collisionBounds.height); i = i + (collisionBounds.height / bounds.height))
		{
			if (collisionLayer.getCell((int) (collisionBounds.x), (int) (i)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingRight(GoldMap map, Entity entity)
	{
		float approx = 1f / (float) map.getTileSize();

		Rectangle bounds = entity.getBounds();
		Rectangle collisionBounds = new Rectangle(bounds.x + entity.getVelocity().x + approx, bounds.y + approx, bounds.width - approx*2, bounds.height - approx*2);

		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) map.getMap().getLayers().get("Collision");
		
		for (float i = collisionBounds.y; i <= (collisionBounds.y + collisionBounds.height); i = i + (collisionBounds.height / bounds.height))
		{
			if (collisionLayer.getCell((int) (collisionBounds.x + collisionBounds.width), (int) (i)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingUp(GoldMap map, Entity entity)
	{
		float approx = 1f / (float) map.getTileSize();

		Rectangle bounds = entity.getBounds();
		Rectangle collisionBounds = new Rectangle(bounds.x + approx*2, bounds.y + entity.getVelocity().y / 4 + approx, bounds.width - approx*4, bounds.height - approx*2);

		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) map.getMap().getLayers().get("Collision");
		
		for (float i = collisionBounds.x; i <= (collisionBounds.x + collisionBounds.width); i = i + (collisionBounds.width / bounds.width))
		{
			if (collisionLayer.getCell((int) (i), (int) (collisionBounds.y + collisionBounds.height)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingDown(GoldMap map, Entity entity)
	{
		float approx = 1f / (float) map.getTileSize();

		Rectangle bounds = entity.getBounds();
		Rectangle collisionBounds = new Rectangle(bounds.x + approx*2, bounds.y + entity.getVelocity().y + approx, bounds.width - approx*4, bounds.height - approx*2);
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) map.getMap().getLayers().get("Collision");
		
		for (float i = collisionBounds.x; i <= (collisionBounds.x + collisionBounds.width); i = i + (collisionBounds.width / bounds.width))
		{
			if (collisionLayer.getCell((int) (i), (int) (collisionBounds.y)) != null)
				return true;
		}
		
		return false;
	}
	
}
