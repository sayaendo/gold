package com.alonjacovi.goldage;

import com.alonjacovi.goldage.Model.Level.GruntField;
import com.alonjacovi.goldage.View.Level.LevelScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector2;

public class GoldGame extends Game {
	
	@Override
	public void create() {		
		//testing - change to go to specific screen
		setScreen(new LevelScreen(GruntField.GruntFieldFactory(new Vector2(5, 17))));
	}
}
