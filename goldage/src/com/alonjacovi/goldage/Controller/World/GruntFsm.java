package com.alonjacovi.goldage.Controller.World;

import com.alonjacovi.goldage.Collision;
import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.EntityState;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.Model.World.Grunt;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class GruntFsm implements EntityFsm
{
	//a basic enemy type that roams around indiscriminately
	//TODO add final int speed and randomization range ints for easy tweaking
	//TODO change the direction polling to entity.isMoving__()
	
	//TODO change grunt to DEAD if he's off screen
	
	private enum GruntState implements EntityState {
		WALKING {

			@Override
			public void enterState(int keycode, GoldMap world,
					Entity entity) {
				//automatic entering on creation until death
			}

			@Override
			public void exitState(int keycode, GoldMap world,
					Entity entity) {
				//no exit logic
			}

			@Override
			public void update(float delta, GoldMap world, Entity entity) 
			{
				Vector2 vel = entity.getVelocity();
				
				//if he's not moving, give him an initial random direction
				if ((vel.x == 0)&&(vel.y == 0))
				{
					switch (MathUtils.random(3))
					{
					case 0: vel.x = -0.25f/2; break;
					case 1: vel.x = 0.25f/2; break;
					case 2: vel.y = 0.25f/2; break;
					case 3: vel.y = -0.25f/2; break;
					}
					
				}
				
				//changing direction chance is 2/120 per frame, or 1 per second (assuming 60fps)
				int random = MathUtils.random(120);
				
				//okay, phew
				//in the direction that the grunt is going
				//check collision
				//then by randomized 1/60 chance, turn clockwise or counter-clockwise
				//then check for collision again in the new direction
				//TODO optimize by avoiding the first collision check if the random seed is a hit
				if (vel.x > 0)
				{
					if (Collision.isCollidingRight(world, entity))
						entity.resetMovement();
					
					if (random == 0)
					{
						entity.resetMovement();
						vel.y = 0.25f/2;
						
						if (Collision.isCollidingUp(world, entity))
							entity.resetMovement();
					
					} else if (random == 1)
					{
						entity.resetMovement();
						vel.y = -0.25f/2;
						
						if (Collision.isCollidingDown(world, entity))
							entity.resetMovement();
					}
				} else if (vel.x < 0)
				{
					if (Collision.isCollidingLeft(world, entity))
						entity.resetMovement();
					
					if (random == 0)
					{
						entity.resetMovement();
						vel.y = 0.25f/2;
						
						if (Collision.isCollidingUp(world, entity))
							entity.resetMovement();
					} else if (random == 1)
					{
						entity.resetMovement();
						vel.y = -0.25f/2;
						
						if (Collision.isCollidingDown(world, entity))
							entity.resetMovement();
					}
				}
				else if (vel.y > 0)
				{
					if (Collision.isCollidingUp(world, entity))
						entity.resetMovement();
					
					if (random == 0)
					{
						entity.resetMovement();
						vel.x = 0.25f/2;
						
						if (Collision.isCollidingRight(world, entity))
							entity.resetMovement();
					} else if (random == 1)
					{
						entity.resetMovement();
						vel.x = -0.25f/2;
						
						if (Collision.isCollidingLeft(world, entity))
							entity.resetMovement();
					}
				} else if (vel.y < 0)
				{
					if (Collision.isCollidingDown(world, entity))
						entity.resetMovement();
					
					if (random == 0)
					{
						entity.resetMovement();
						vel.x = 0.25f/2;
						
						if (Collision.isCollidingRight(world, entity))
							entity.resetMovement();
					} else if (random == 1)
					{
						entity.resetMovement();
						vel.x = -0.25f/2;
						
						if (Collision.isCollidingLeft(world, entity))
							entity.resetMovement();
					}
				}
				
			}
			
		},
		
		DEAD {

			@Override
			public void enterState(int keycode, GoldMap world, Entity entity) {
				Grunt grunt = (Grunt) entity;
				grunt.getFsm(0).setState(this);
			}

			@Override
			public void exitState(int keycode, GoldMap world,
					Entity entity) {
				//grunt is dead and gone upon enterState() forever
			}

			@Override
			public void update(float delta, GoldMap world, Entity entity) {
				//grunt is dead and gone upon enterState() forever
			}
			
		};
		
		@Override
		public abstract void enterState(int keycode, GoldMap world, Entity entity);
		@Override
		public abstract void exitState(int keycode, GoldMap world, Entity entity);
		@Override
		public abstract void update(float delta, GoldMap world, Entity entity);
		
	}
	
	private GruntState state = GruntState.WALKING;
	private Rectangle spawnArea;
	private Grunt grunt;
	
	private float spawnTimer;
	
	public GruntFsm(RectangleMapObject spawnAreaObject, int tileSize) {
		spawnArea = spawnAreaObject.getRectangle();
		
		//shrink the spawn area to the internal grid
		spawnArea.x = spawnArea.x / tileSize;
		spawnArea.y = spawnArea.y / tileSize;
		spawnArea.height = spawnArea.height / tileSize;
		spawnArea.width = spawnArea.width / tileSize;
		
		//randomize a location inside the spawn area and create a grunt in that position
		Vector2 startingPosition = new Vector2(MathUtils.random(spawnArea.x, spawnArea.x+spawnArea.width),
												MathUtils.random(spawnArea.y, spawnArea.y+spawnArea.height));
		
		grunt = Grunt.GruntFactory(startingPosition, this);
	}
	
	//TODO wait what
	public Grunt getControlled() {
		return grunt;
	}

	@Override
	public EntityState getState() {
		return state;
	}
	
	//functionally the same as spawnTimer, however spawnTimer is for internal logic and separate from the animation timer
	private float timer;
	
	@Override
	public float getTimer() {
		return timer;
	}
	
	@Override
	public void resetTimer() {
		timer = 0;
	}

	@Override
	public void setState(EntityState state) {
		this.state = (GruntState) state;
		
		timer = 0;
	}

	@Override
	public void handleKeyPressed(int keycode, GoldMap world,
			Entity entity) {
		throw new UnsupportedOperationException("WorldGrunt is not controlled by input");
	}

	@Override
	public void handleKeyReleased(int keycode, GoldMap world,
			Entity entity) {
		throw new UnsupportedOperationException("WorldGrunt is not controlled by input");
	}

	@Override
	public void update(float delta, GoldMap world, Entity entity) {
		//kill grunt after 20 seconds
		spawnTimer += delta;
		if (spawnTimer > 20)
		{
			spawnTimer = 20;
			//commenting this out for easier testing
			//WorldGruntState.DEAD.enterState(0, world, entity);
		}
		
		timer += delta;
		
		state.update(delta, world, entity);
		
		entity.updatePosition(delta);
	}

	
	//testing
	@Override
	public EntityState getPreviousState() {
		// TODO Auto-generated method stub
		return null;
	}

}
