package com.alonjacovi.goldage.Controller.World;

import com.alonjacovi.goldage.Collision;
import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.EntityState;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.Model.World.Player;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

public class PlayerFsm implements EntityFsm
{
	public enum PlayerState implements EntityState 
	{
		//(WorldPlayer) entity cast is safe, as WorldPlayer is the only entity with access to this FSM
		
		//TODO fix the bug where after sliding the player gets collided a bit off-center
		//i think it happens every time you move both vertically and horizontally while sliding once
		
		//TODO change the timer reset to the setState method?
		
		//TODO clean up the code changing state (the if statements in handleInput and the idle check in checkTerrain
		
		//TODO change the casts to no casts, using the interface-defined variable is better
		
			MOVING_SOIL
			{
				@Override
				public void enterState(int keycode, GoldMap world, Entity entity) 
				{	
					Player player = (Player) entity;
					player.getFsm(0).setState(this);
					player.getFsm(0).resetTimer();
					
					//depending on input pressed, put player on constant speed
					switch(keycode)
					{
					case Keys.LEFT: 
						player.resetMovement();
						player.getVelocity().x = -maxSpeed;
						break;
					case Keys.RIGHT: 
						player.resetMovement();
						player.getVelocity().x = maxSpeed;
						break;
					case Keys.UP: 
						player.resetMovement();
						player.getVelocity().y = maxSpeed;
						break;
					case Keys.DOWN: 
						player.resetMovement();
						player.getVelocity().y = -maxSpeed; 
						break;
					}
				}

				@Override
				public void exitState(int keycode, GoldMap world, Entity entity) 
				{
					Player player = (Player) entity;
					Vector2 vel = player.getVelocity();
					
					//depending on input released, if player is moving in the input's direction, stop player
					switch(keycode)
					{
					case Keys.LEFT: 
						if (vel.x < 0)
							player.resetMovement();
						break;
					case Keys.RIGHT: 
						if (vel.x > 0) 
							player.resetMovement();
						break;
					case Keys.UP:
						if (vel.y > 0)
							player.resetMovement();
						break;
					case Keys.DOWN:
						if (vel.y < 0)
							player.resetMovement();
						break;
					}
				}

				@Override
				public void update(float delta, GoldMap world, Entity entity)
				{					
					Player player = (Player) entity;
					Vector2 vel = player.getVelocity();
					
					//collision checking only for the tile in the direction the player is moving
					if (vel.x < 0)
					{
						if (Collision.isCollidingLeft(world, entity))
							player.resetMovement();
					} else if (vel.x > 0)
					{
						if (Collision.isCollidingRight(world, entity))
							player.resetMovement();
					} else if (vel.y > 0)
					{
						if (Collision.isCollidingUp(world, entity))
							player.resetMovement();
					} else if (vel.y < 0)
					{
						if (Collision.isCollidingDown(world, entity))
							player.resetMovement();
					}
					
				}
			},
			
			MOVING_ICE
			{

				@Override
				public void enterState(int keycode, GoldMap world, Entity entity) 
				{
					
					
					Player player = (Player) entity;
					player.getFsm(0).setState(this);
					
					player.getFsm(0).resetTimer();
					
					//depending on input, add acceleration in the corresponding direction
					//if player isn't moving in that direction, add some starting movement
					//if he's already moving, the two directions will combine
					switch(keycode)
					{
					case Keys.LEFT: 
						player.getAcceleration().x = -maxSpeed/16; 
						if (player.getVelocity().x == 0)
							player.getVelocity().x = -maxSpeed/16; 
						break;
					case Keys.RIGHT: 
						player.getAcceleration().x = maxSpeed/16; 
						if (player.getVelocity().x == 0)
							player.getVelocity().x = maxSpeed/16; 
						break;
					case Keys.UP: 
						player.getAcceleration().y = maxSpeed/16;
						if (player.getVelocity().y == 0)
							player.getVelocity().y = maxSpeed/16; 
						break;
					case Keys.DOWN: 
						player.getAcceleration().y = -maxSpeed/16; 
						if (player.getVelocity().y == 0)
							player.getVelocity().y = -maxSpeed/16; 
						break;
					}
				}

				@Override
				public void exitState(int keycode, GoldMap world, Entity entity) 
				{	
					Player player = (Player) entity;	
					Vector2 vel = player.getVelocity();
					Vector2 acc = player.getAcceleration();
					
					//depending on input,
					//if player is moving in the corresponding direction,
					//AND if he's accelerating in that direction
					//reverse acceleration
					switch(keycode)
					{
					case Keys.LEFT: 
						if (vel.x < 0)
							if (acc.x < 0)
								player.getAcceleration().x = - player.getAcceleration().x;
						break;
					case Keys.RIGHT: 
						if (vel.x > 0)
							if (acc.x > 0)
								player.getAcceleration().x = - player.getAcceleration().x;
						break;
					case Keys.UP:
						if (vel.y > 0)	
							if (acc.y > 0)
								player.getAcceleration().y = - player.getAcceleration().y;
						break;
					case Keys.DOWN:
						if (vel.y < 0)
							if (acc.y < 0)
								player.getAcceleration().y = - player.getAcceleration().y;
						break;
					}
				}

				@Override
				public void update(float delta, GoldMap world, Entity entity) 
				{	
					Player player = (Player) entity;
					Vector2 vel = player.getVelocity();
					Vector2 acc = player.getAcceleration();
					
					//collision checking
					if (vel.x < 0)
					{
						if (Collision.isCollidingLeft(world, entity))
							player.resetMovement();
					} else if (vel.x > 0)
					{
						if (Collision.isCollidingRight(world, entity))
							player.resetMovement();
					} else if (vel.y > 0)
					{
						if (Collision.isCollidingUp(world, entity))
							player.resetMovement();
					} else if (vel.y < 0)
					{
						if (Collision.isCollidingDown(world, entity))
							player.resetMovement();
					}
					
					//if the acceleration is reversed (meaning, exitState() was called)
					//AND if the player's movement stopped as a result
					//stop the acceleration
					//essentially treating the reversed acceleration as friction
					//TODO poll for key pressed. if key is in the direction of the reversed acceleration,
					//don't check for no movement, and don't reset the acceleration
					if (acc.x > 0)
					{
						if (!Gdx.input.isKeyPressed(Keys.LEFT))
							if (vel.x == 0)
								acc.x = 0;
					} else if (acc.x < 0)
					{
						if (!Gdx.input.isKeyPressed(Keys.RIGHT))
							if (vel.x == 0)
								acc.x = 0;
					}
					if (acc.y > 0)
					{
						if (!Gdx.input.isKeyPressed(Keys.DOWN))
							if (vel.y == 0)
								acc.y = 0;
					} else if (acc.y < 0)
					{
						if (!Gdx.input.isKeyPressed(Keys.UP))
							if (vel.y == 0)
								acc.y = 0;
					}
				}
				
			},
			
			IDLE {
				//passive state. enters and exits automatically as player is/isn't idle as controlled by the other states
				
				@Override
				public void enterState(int keycode, GoldMap world, Entity entity) {
					Player player = (Player) entity;
					player.getFsm(0).setState(this);
					
					player.getFsm(0).resetTimer();
				}

				@Override
				public void exitState(int keycode, GoldMap world, Entity entity) {
				
				}

				@Override
				public void update(float delta, GoldMap world, Entity entity) {

				}
			};
			
			private final static float maxSpeed = 0.25f/2;

			@Override
			public abstract void enterState(int keycode, GoldMap world, Entity entity);
			@Override
			public abstract void exitState(int keycode, GoldMap world, Entity entity);
			@Override
			public abstract void update(float delta, GoldMap world, Entity entity);
	};
	
	private PlayerState state = PlayerState.IDLE;
	

	@Override
	public EntityState getState() {
		return state;
	}
	
	@Override
	public void setState(EntityState state) {
		this.state = (PlayerState) state;
	}
	
	private float timer;
	
	public float getTimer() {
		return timer;
	}
	
	@Override
	public void resetTimer() {
		timer = 0;
	}
	

	@Override
	public void handleKeyPressed(int keycode, GoldMap world, Entity entity) {
		
		//if pressed in one of the directions, enter the MOVING state
		//because checkTerrain is called after the check if player is idle,
		//the state entered from here will always be a moving state
		if (state == PlayerState.IDLE)
			state = checkTerrain(world, (Player) entity);
		switch(keycode)
		{
		case Keys.LEFT:
		case Keys.RIGHT:
		case Keys.UP:
		case Keys.DOWN: state.enterState(keycode, world, entity); break;
		}
	}

	@Override
	public void handleKeyReleased(int keycode, GoldMap world, Entity entity) {
		
		//if pressed in one of the directions, exit the MOVING state
		//because checkTerrain is called after the check if player is idle,
		//the state exited from here will always be a moving state
		if (state == PlayerState.IDLE)
			state = checkTerrain(world, (Player) entity);
		switch(keycode)
		{
		case Keys.LEFT:
		case Keys.RIGHT:
		case Keys.UP:
		case Keys.DOWN: state.exitState(keycode, world, entity); break;
		}
	}

	@Override
	public void update(float delta, GoldMap world, Entity entity) 
	{
		//call update on the current state
		state.update(delta, world, entity);
		
		timer += delta;
		
		Player player = (Player) entity;
		Vector2 vel = player.getVelocity();
		Vector2 acc = player.getAcceleration();
		
		//if player is idle, enter IDLE state
		if ((acc.x == 0)&&(acc.y == 0)&&(vel.x == 0)&&(vel.y == 0))
			if (state != PlayerState.IDLE)
				PlayerState.IDLE.enterState(0, world, entity);
		
		
		//if player changed terrains, AND he's already moving
		//then enter the new terrain with the input of that direction
		//if player is de-accelerating on the transfer, reset his movement instead
		PlayerState newTerrain;
		
		if (state != ( newTerrain = checkTerrain(world, player)))
		{
			if (state != PlayerState.IDLE)
				state = newTerrain;
			if ((vel.x > 0)&&(acc.x >=0))
				state.enterState(Keys.RIGHT, world, entity);
			else if ((vel.x < 0)&&(acc.x <=0))
				state.enterState(Keys.LEFT, world, entity);
			else if ((vel.y > 0)&&(acc.y >=0))
				state.enterState(Keys.UP, world, entity);
			else if ((vel.y < 0)&&(acc.y <=0))
				state.enterState(Keys.DOWN, world, entity);
			else {
				player.resetMovement();
				//WorldPlayerMovementState.IDLE.enterState(0, world, entity);
			}
		}

		
		//update player's position
		player.updatePosition(delta);
	}
	
	public PlayerState checkTerrain(GoldMap world, Player player) 
	{
		//adding half a unit to player's position to check by the middle of his location, not bottom left
		int tileX = (int) (player.getPosition().x + 0.5);
		int tileY = (int) (player.getPosition().y + 0.5);

		TiledMapTileLayer areaLayer = (TiledMapTileLayer) world.getMap().getLayers().get("Floor");
		TiledMapTile tile = areaLayer.getCell(tileX, tileY).getTile();
		String type = tile.getProperties().get("type", String.class);
		if (type != null)
		{			
			if (type.equals("GRASS"))
				return PlayerState.MOVING_SOIL;
			else if (type.equals("ICE"))
				return PlayerState.MOVING_ICE;
		} else
			return PlayerState.MOVING_SOIL;
		
		throw new IllegalArgumentException("TMX map Background layer is not fully covered");
		
		//TODO change "type" to "terrain"
		//TODO change "background" to "floor"
	}

	
	
	//testing
	@Override
	public EntityState getPreviousState() {
		// TODO Auto-generated method stub
		return null;
	}




	
	

}
