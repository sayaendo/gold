package com.alonjacovi.goldage.Controller.Level;

import java.util.LinkedList;

import com.alonjacovi.goldage.Collision;
import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.EntityState;
import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Level.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class PlayerFsm implements EntityFsm
{
	
	//TODO add Air Suspension state
	//TODO add walking off a ledge (goes into jumping by setting state i guess)
	
	private final static float maxSpeed = 1/10f;
	private final static float jumpSpeed = 1/3.76f;
	private final static float superJump = 1/3.32f;
	
	public enum PlayerMovementState implements EntityState 
	{
		WALKING_LEFT {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) 
			{
				entity.getFsm(0).setState(this);
				
				if (keycode == Keys.LEFT)
				{
					if (entity.getFsm(0).getPreviousState() == WALKING_RIGHT)
					{
						entity.getAcceleration().x = -maxSpeed/5;
					} 
					else entity.getAcceleration().x = -maxSpeed/8;
				}
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) 
			{	
				
				if (keycode == Keys.LEFT)
				{
					entity.getAcceleration().x = 0;
				}
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) 
			{
				if (entity.isCollidingLeft())
					IDLE.enterState(0, map, entity);
				
				if (!entity.isCollidingDown())
					entity.getFsm(0).setState(JUMPING);
					
			}
		},
		WALKING_RIGHT {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) 
			{
				entity.getFsm(0).setState(this);
				
				if (keycode == Keys.RIGHT)
				{
					if (entity.getFsm(0).getPreviousState() == WALKING_LEFT)
					{
						entity.getAcceleration().x = maxSpeed/5;
					} 
					else entity.getAcceleration().x = maxSpeed/8;
				}
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) 
			{	
				
				if (keycode == Keys.RIGHT)
				{
					entity.getAcceleration().x = 0;
				}
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) 
			{
				if (entity.isCollidingRight())
					IDLE.enterState(0, map, entity);
				
				if (!entity.isCollidingDown())
					entity.getFsm(0).setState(JUMPING);
			}
		},
		JUMPING {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) 
			{
				entity.getFsm(0).setState(this);

				//remove player's acceleration at the start of a jump,
				//making the jump only carry horizontal velocity equal
				//to when the jump started, throughout it
				entity.getAcceleration().x = 0;

				//if player is currently moving in his max speed
				//make him jump higher
				if (Math.abs(entity.getVelocity().x) >= maxSpeed - Constants.FRICTION.get())
					entity.getVelocity().y = superJump;
				else entity.getVelocity().y = jumpSpeed;

			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) 
			{
				//if the jump button was released mid-jump,
				//clamp velocity to jumpSpeed (only applies to superJump)
				//note the multiplier is only for frame adjusting
				if (entity.getVelocity().y > jumpSpeed * 0.72f)
					entity.getVelocity().y = jumpSpeed * 0.72f;
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) 
			{	
				//left and right movement while in the air
				//basically the same as Walking
				if (!Gdx.input.isKeyPressed(Keys.DOWN))
				{
					if (Gdx.input.isKeyPressed(Keys.LEFT))
						entity.getAcceleration().x = -maxSpeed/8;
					else if (Gdx.input.isKeyPressed(Keys.RIGHT))
						entity.getAcceleration().x = maxSpeed/8;
					else entity.getAcceleration().x = 0;
				}
				
				if (entity.isCollidingUp())
					AIR_SUSPEND.enterState(0, map, entity);
				
				if (entity.isCollidingDown())
				{
					//If A was let go mid-jump and the jump was stationary, it's a small hop,
					//and it goes straight to CROUCHING or IDLE depending on if Down is pressed.
					if (((Gdx.input.isKeyPressed(Keys.A))||(entity.getVelocity().x != 0))&&
							(entity.getVelocity().y <= -jumpSpeed))
					{	
						entity.getVelocity().y = 0;
						LANDING.enterState(0, map, entity); }
					else { 
						entity.getVelocity().y = 0;
						if (Gdx.input.isKeyPressed(Keys.DOWN))
							CROUCHING.enterState(Keys.DOWN, map, entity);
						else {
							if (Gdx.input.isKeyPressed(Keys.LEFT))
								WALKING_LEFT.enterState(Keys.LEFT, map, entity);
							else if (Gdx.input.isKeyPressed(Keys.RIGHT))
								WALKING_RIGHT.enterState(Keys.RIGHT, map, entity);
							else IDLE.enterState(0, map, entity);
						}
					}
				}
			}
		},
		AIR_SUSPEND {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				entity.getFsm(0).setState(this);
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				entity.getVelocity().y += Constants.GRAVITY.get();
				
				if (entity.getFsm(0).getTimer() >= 0.25f/2)
					entity.getFsm(0).setState(JUMPING);
				
				//left and right movement while in the air
				//basically the same as Walking
				if (!Gdx.input.isKeyPressed(Keys.DOWN))
				{
					if (Gdx.input.isKeyPressed(Keys.LEFT))
						entity.getAcceleration().x = -maxSpeed/8;
					else if (Gdx.input.isKeyPressed(Keys.RIGHT))
						entity.getAcceleration().x = maxSpeed/8;
					else entity.getAcceleration().x = 0;
				}
			}
			
		},
		LANDING {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) 
			{
				entity.getFsm(0).setState(this);
				
				if (Gdx.input.isKeyPressed(Keys.DOWN))
					CROUCHING.enterState(Keys.DOWN, map, entity);
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {

			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) 
			{
				//after half a second in Landing, go to Idle
				if (entity.getFsm(0).getTimer() > 0.25f/2)
					if (entity.isMovingLeft()) {
						entity.getFsm(0).setState(WALKING_LEFT);
					} else {
						if (entity.isMovingRight())
							entity.getFsm(0).setState(WALKING_RIGHT);
						else IDLE.enterState(0, map, entity);
					}
			}

		},
		CROUCHING {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) 
			{
				entity.getFsm(0).setState(this);
				
				entity.getAcceleration().x = 0;			
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) 
			{
				IDLE.enterState(keycode, map, entity);
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {	
			}
		},
		IDLE {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				entity.getFsm(0).setState(this);
				
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity)
			{
				entity.getAcceleration().x = 0;
			}
		},
		WINDUP {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

		},
		RECOIL {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

		},
		ATTACKING {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

		},
		CROUCH_ATTACKING {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub

			}
		};

		@Override
		public abstract void enterState(int keycode, GoldMap map, Entity entity);
		@Override
		public abstract void exitState(int keycode, GoldMap map, Entity entity);
		@Override
		public abstract void update(float delta, GoldMap map, Entity entity);

	}
	

	private LinkedList<PlayerMovementState> history = new LinkedList<PlayerMovementState>();

	public PlayerFsm() {
		history.add(PlayerMovementState.IDLE);
	}

	@Override
	public EntityState getState() {
		return history.peekLast();
	}

	@Override
	public EntityState getPreviousState() {
		return history.peekFirst();
	}

	@Override
	public void setState(EntityState state) {
		if (history.size() > 2)
			history.pop();
		history.add((PlayerMovementState) state);

		resetTimer();
	}

	private float timer;

	@Override
	public float getTimer() {
		return timer;
	}

	@Override
	public void resetTimer() {
		timer = 0;
	}

	@Override
	public void handleKeyPressed(int keycode, GoldMap map, Entity entity) 
	{		
		EntityState state = getState();
		
		switch (keycode)
		{
		case Keys.A:
			if (((state == PlayerMovementState.LANDING)||
			 	 (state == PlayerMovementState.CROUCHING)||
				 (state == PlayerMovementState.IDLE)||
				 (state == PlayerMovementState.WALKING_LEFT)||
				 (state == PlayerMovementState.WALKING_RIGHT)||
				 (state == PlayerMovementState.RECOIL)||
				 (state == PlayerMovementState.WINDUP))&&
				 (entity.isCollidingDown()))

				PlayerMovementState.JUMPING.enterState(keycode, map, entity);
			break;
			
		case Keys.RIGHT:
			
			if (((state == PlayerMovementState.LANDING)||
				 (state == PlayerMovementState.IDLE)||
				 (state == PlayerMovementState.WALKING_LEFT)||
				 (state == PlayerMovementState.WALKING_RIGHT)||
				 (state == PlayerMovementState.RECOIL))&&
				(!entity.isCollidingRight()))
					
				PlayerMovementState.WALKING_RIGHT.enterState(keycode, map, entity);
			break;
		case Keys.LEFT:
			
			if (((state == PlayerMovementState.LANDING)||
				 (state == PlayerMovementState.IDLE)||
				 (state == PlayerMovementState.WALKING_LEFT)||
				 (state == PlayerMovementState.WALKING_RIGHT)||
				 (state == PlayerMovementState.RECOIL))&&
				(!entity.isCollidingLeft()))
								
				PlayerMovementState.WALKING_LEFT.enterState(keycode, map, entity);
			break;
			
		case Keys.DOWN: 
			
			if ((state == PlayerMovementState.LANDING)||
				(state == PlayerMovementState.IDLE)||
				(state == PlayerMovementState.RECOIL)||
				(state == PlayerMovementState.WALKING_LEFT)||
				(state == PlayerMovementState.WALKING_RIGHT))
				
				PlayerMovementState.CROUCHING.enterState(keycode, map, entity);
			break;
		}
	}

	@Override
	public void handleKeyReleased(int keycode, GoldMap map, Entity entity) 
	{
		EntityState state = getState();
		
		switch (keycode)
		{
		case Keys.A: 
			if (state == PlayerMovementState.JUMPING)
				
				PlayerMovementState.JUMPING.exitState(keycode, map, entity);
			break;
		case Keys.RIGHT:
			if ((state == PlayerMovementState.WALKING_RIGHT)||
				(state == PlayerMovementState.LANDING))

				PlayerMovementState.WALKING_RIGHT.exitState(keycode, map, entity);
			break;
		case Keys.LEFT: 
			if ((state == PlayerMovementState.WALKING_LEFT)||
				(state == PlayerMovementState.LANDING))
				
				PlayerMovementState.WALKING_LEFT.exitState(keycode, map, entity);
			break;
		case Keys.DOWN:
			if (state == PlayerMovementState.CROUCHING)
				
				PlayerMovementState.CROUCHING.exitState(keycode, map, entity);		
			break;
		}
	}

	@Override
	public void update(float delta, GoldMap map, Entity entity) 
	{
		timer += delta;
		
		getState().update(delta, map, entity);
		
		if (entity.isCollidingLeft())
		{
			//entity.getAcceleration().x = 0;
			if (!entity.isCollidingUp())
				entity.getVelocity().x = 0;
		}
		
		if (entity.isCollidingRight())
		{
			//entity.getAcceleration().x = 0;
			if (!entity.isCollidingUp())
				entity.getVelocity().x = 0;
		}

		if (entity.isCollidingUp())
		{
			//entity.getAcceleration().y = 0;
			entity.getVelocity().y = 0;
		}
		
		if (entity.isCollidingDown())
		{
			//entity.getAcceleration().y = 0;
			if (getState() != PlayerMovementState.JUMPING)
				entity.getVelocity().y = 0;
			
			//friction
			if (entity.getVelocity().x != 0)
			{ 
				if (Math.abs(entity.getVelocity().x) > Constants.FRICTION.get())
				{
					entity.getVelocity().x += Constants.FRICTION.get() * (- Math.signum(entity.getVelocity().x));
				}
				else {
					if (((getState() != PlayerMovementState.WALKING_RIGHT)||
							(!Gdx.input.isKeyPressed(Keys.RIGHT)))&&
							((getState() != PlayerMovementState.WALKING_LEFT)||
									(!Gdx.input.isKeyPressed(Keys.LEFT))))
					{
						entity.getVelocity().x = 0;
						if (getState() != PlayerMovementState.CROUCHING)
							PlayerMovementState.IDLE.enterState(0, map, entity);
					}
				}
			}
		}
		
		//updating player position and velocity, and clamping velocity
		Vector2 vel = entity.getVelocity();
		
		entity.getPosition().add(vel);
		vel.add(entity.getAcceleration());
		vel.y -= Constants.GRAVITY.get();
		
		if (vel.x > maxSpeed) vel.x = maxSpeed;
		if (vel.y > superJump) vel.y = superJump; 
		if (vel.x < -maxSpeed) vel.x = -maxSpeed;
		if (vel.y < -superJump) vel.y = -superJump; 
		
		
	}

}
