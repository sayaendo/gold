package com.alonjacovi.goldage.Controller.Level;

import com.alonjacovi.goldage.Controller.EntityFsm;
import com.alonjacovi.goldage.Controller.EntityState;
import com.alonjacovi.goldage.Model.Entity;
import com.alonjacovi.goldage.Model.GoldMap;

public class PlayerWeaponFsm implements EntityFsm
{
	
	//this FSM flips between weapons passively, they're changed through the level menu
	//or through a hotkey which is defined here, cycles through the weapons
	//when i have weapon inventory up, the hotkey must check for which weapon is available to the player
	
	//helper method takes a PlayerWeaponState and entity, and returns the weapon bounds using entity.getBounds and entity.getDirection
	//the attack checking is in the player fsm attack and crouch attack
	//crouch attack takes the hitbox and lowes it by a tile (.x -= 1)
	
	//AbstractLevelEntity which has health and a hit(int attackPower) method
	//when an FSM hits an entity, it calls on the entity's hit method
	//the entity's hit method calls on its FSM's gotHit method
	
	public enum PlayerWeaponState implements EntityState {
		SWORD {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}
		},
		SPEAR {

			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}
			
		},
		NONE {
			@Override
			public void enterState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void exitState(int keycode, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(float delta, GoldMap map, Entity entity) {
				// TODO Auto-generated method stub
				
			}
		};
		
		@Override
		public abstract void enterState(int keycode, GoldMap map, Entity entity);
		@Override
		public abstract void exitState(int keycode, GoldMap map, Entity entity);
		@Override
		public abstract void update(float delta, GoldMap map, Entity entity);
	}

	@Override
	public EntityState getState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntityState getPreviousState() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setState(EntityState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public float getTimer() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void resetTimer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleKeyPressed(int keycode, GoldMap map, Entity entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleKeyReleased(int keycode, GoldMap map, Entity entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(float delta, GoldMap map, Entity entity) {

	}



}
