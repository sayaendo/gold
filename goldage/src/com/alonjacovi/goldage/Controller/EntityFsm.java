package com.alonjacovi.goldage.Controller;

import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;
import com.badlogic.gdx.math.Vector2;

public interface EntityFsm
{
	public EntityState getState();
	public void setState(EntityState state);
	public EntityState getPreviousState();
	
	public float getTimer();
	public void resetTimer();
	
	public void handleKeyPressed(int keycode, GoldMap map, Entity entity);
	public void handleKeyReleased(int keycode, GoldMap map, Entity entity);

	public void update(float delta, GoldMap map, Entity entity);
}
