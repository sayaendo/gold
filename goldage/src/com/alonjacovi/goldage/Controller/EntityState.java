package com.alonjacovi.goldage.Controller;

import com.alonjacovi.goldage.Model.GoldMap;
import com.alonjacovi.goldage.Model.Entity;

public interface EntityState
{
	
	public abstract void enterState(int keycode, GoldMap map, Entity entity);
	public abstract void exitState(int keycode, GoldMap map, Entity entity);
	
	public abstract void update(float delta, GoldMap map, Entity entity);

}
