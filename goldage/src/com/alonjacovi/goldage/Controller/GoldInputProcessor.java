package com.alonjacovi.goldage.Controller;

import com.alonjacovi.goldage.Model.GoldMap;
import com.badlogic.gdx.InputProcessor;

public class GoldInputProcessor implements InputProcessor
{
	GoldMap map;
	
	public GoldInputProcessor(GoldMap map) {
		this.map = map;
	}

	@Override
	public boolean keyDown(int keycode)
	{
		map.handleKeyPressed(keycode);
		return true;
	}

	@Override
	public boolean keyUp(int keycode)
	{		
		map.handleKeyReleased(keycode);
		return true;
	}
	
	//World doesn't support character inputs
	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}
	
	//no android support - no implementation
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		return false;
	}

}
