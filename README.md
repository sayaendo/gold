# GoldAge #

(on hiatus as I moved to Unity)

##Overview##

This is an engine I've written in LibGDX that mimics the action-platforming gameplay of The Legend of Zelda II. The state flow of the original gameplay, which was emulated here, is as such:

![2014-03-26 19.52.14.jpg](https://bitbucket.org/repo/MaBXgz/images/2728182521-2014-03-26%2019.52.14.jpg)

[The full state machine code is here.](https://bitbucket.org/sayaendo/gold/src/850570c95f2b86593ca05e179110662e69588553/goldage/src/com/alonjacovi/goldage/Controller/Level/PlayerFsm.java?at=master)

The player gameplay is complete, with walking, dynamic-height jumping, and sword gameplay. The collision detection and physics module are also complete and mimic the original physics in the original game. There are still no enemies and the levels are read from a TMX file.

The engine contains two modes: top-down gameplay where the player roams around an overworld, and the sidescrolling action gameplay, similar to the original Zelda game.

[Here's a video showcasing the sidescrolling gameplay](https://www.youtube.com/watch?v=ogx4G2kHLOg)